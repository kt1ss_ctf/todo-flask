from flask import Flask, render_template, request, redirect
import json
import os

app = Flask(__name__)

DB_FILE = os.path.join(os.path.dirname(__file__), 'tasks.json')

@app.route('/')
def index():
    with open(DB_FILE) as f:
        tasks = json.load(f)

    return render_template('index.html', tasks=tasks)

@app.route('/add', methods=['POST'])
def add_task():
    task_name = request.form.get('name')

    with open(DB_FILE, 'r') as f:
        tasks = json.load(f)

    new_id = tasks[-1]['id'] + 1
    tasks.append({
        "id": new_id,
        "name": task_name
    })

    with open(DB_FILE, 'w') as f:
        json.dump(tasks, f, indent=4)

    return redirect('/')

@app.route('/delete/<int:id>')
def delete(id):
    with open(DB_FILE, 'r') as f:
        tasks = json.load(f)

    tasks = [x for x in tasks if x['id'] != id]

    with open(DB_FILE, 'w') as f:
        json.dump(tasks, f, indent=4)

    return redirect('/')
